import React, {useState} from 'react';


function DollarToEuroButton (props){

    let {dollarToEuro} = props


    return <div>
        <button onClick = {dollarToEuro}>Convert US Dollar To Euro</button>
    </div>

}


export default DollarToEuroButton