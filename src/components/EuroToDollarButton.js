import React, {useState} from 'react';


function EuroToDollarButton (props){

    let {euroToDollar} = props


    return <div>
    
        <button onClick = {euroToDollar}>Convert Euro To US Dollar</button>    
    </div>

}


export default EuroToDollarButton