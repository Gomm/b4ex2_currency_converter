import React, {useState, useEffect} from 'react';
import './App.css';
import Converter from './components/Converter';
import DollarToEuroButton from './components/DollarToEuroButtton';
import EuroToDollarButton from './components/EuroToDollarButton';




function App() {

      const [converter, setConverter] = useState ('');
      const [converted, setConverted] = useState ('');
      const [rate, setRate] = useState (0);
     
      useEffect( () => {
          getData()
      },[])

      const getData = () => {
        fetch(`http://www.apilayer.net/api/live?access_key=ca570b58d0ed1d2ae71d2543bfc55154`) 
        .then( res => res.json())
        .then( response => {
          console.log(response) 
          let tempRate = rate
          tempRate = response.quotes.USDEUR
          setRate (tempRate)
        })
        .catch( error => console.log(error))
      }


      const handleChangeConverter = (e) => {
        let tempConverter = converter
        tempConverter = e.target.value
        setConverter(tempConverter);
          console.log (tempConverter);
       // reset the state here ????
      };

      const buttonClickedEuroToDollar = ()=> {   
        //debugger       
          let tempConverter = converter  / rate;
         return  setConverted ('$ ' + tempConverter.toFixed(2));
      };

      const buttonClickedDollarToEuro = () => {
          let  tempConverter = converter * rate;
          return setConverted ('€ ' + tempConverter.toFixed(2));
          
      };

      

      return (
        <div className="App">
          <header className="App-header">
            

           
            <Converter convert = {handleChangeConverter} converted={converted} />
            <DollarToEuroButton dollarToEuro = {buttonClickedDollarToEuro}/>
            <EuroToDollarButton euroToDollar = {buttonClickedEuroToDollar} />


          </header>
        </div>
      );
}

export default App;
